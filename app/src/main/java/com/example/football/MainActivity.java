package com.example.football;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer countOne = 0;
    private Integer countTwo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onResume() {
        super.onResume();
        resetUI();
    }

    @SuppressLint("SetTextI18n")
    public void onClickButton(View view) {

        switch (view.getId()) {
            case R.id.buttonTwo: {
                countOne++;
                TextView counterViev = findViewById(R.id.counterOne);
                counterViev.setText(countOne.toString());
                break;
            }
            case R.id.buttonThree: {
                countTwo++;
                TextView counterViev = findViewById(R.id.counterTwo);
                counterViev.setText(countTwo.toString());
                break;
            }
            case R.id.buttonOne: {
                countOne = 0;
                countTwo = 0;
                TextView counterVievs = findViewById(R.id.counterOne);
                counterVievs.setText(countOne.toString());
                TextView counterViev = findViewById(R.id.counterTwo);
                counterViev.setText(countTwo.toString());
                break;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("countOne", countOne);
        outState.putInt("countTwo", countTwo);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("countOne")) {
            countOne = savedInstanceState.getInt("countOne");
        }
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("countOne")) {
            countTwo = savedInstanceState.getInt("countTwo");
        }
    }

    @SuppressLint("SetTextI18n")
    private void resetUI() {
        ((TextView) findViewById(R.id.counterOne)).setText(countOne.toString());
        ((TextView) findViewById(R.id.counterTwo)).setText(countTwo.toString());
    }
}
